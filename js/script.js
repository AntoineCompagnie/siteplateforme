		$(document).ready(function(){
			$('#mycarousel').carousel({interval:2000});
		    $("#carousel-button").on("click",function(){
		        if($("#carousel-button").children("span").hasClass('fa-pause')){
		            $("#carousel-button").carousel('pause');
		            $("#carousel-button").children("span").removeClass('fa-pause');
		            $("#carousel-button").children("span").addClass('fa-play');
		        }else if($("#carousel-button").children("span").hasClass('fa-play')){
		            $("#carousel-button").carousel('cycle');
		            $("#carousel-button").children("span").removeClass('fa-play');
		            $("#carousel-button").children("span").addClass('fa-pause');
		        }
		    });
		});
